using DI.Factory;

namespace DI.Container
{
    public class SimpleContext : IContext
    {
        private readonly IDIContainer m_container = new DIContainer();

        public void Register<TType, TImplementation>()
        {
            m_container.Register<TType, TImplementation>();
        }

        public void Register<TType, TImplementation>(IDIFactory<TImplementation> factory)
        {
            m_container.Register<TType, TImplementation>(factory);
        }

        public void RegisterConditional<TType, TImplementation>(IDIFactory<TImplementation> factory, int hashCode)
        {
            m_container.RegisterConditional<TType, TImplementation>(factory, hashCode);
        }

        public void RegisterInstance<TType, TImplementation>() where TImplementation : TType, new()
        {
            m_container.RegisterInstance<TType, TImplementation>();
        }

        public void RegisterInstance<TType, TImplementation>(TImplementation instance) where TImplementation : TType
        {
            m_container.RegisterInstance<TType, TImplementation>(instance);
        }

        public void RegisterInstanceConditional<TType, TImplementation>(TImplementation instance, int hashCode)
        {
            m_container.RegisterInstanceConditional<TType, TImplementation>(instance, hashCode);
        }

        public void Unregister<TType>()
        {
            m_container.Unregister<TType>();
        }

        public void UnregisterConditional<TType>(int hashCode)
        {
            m_container.UnregisterConditional<TType>(hashCode);
        }

        public TType Get<TType>()
        {
            return m_container.Get<TType>();
        }

        public TType GetConditional<TType>(int hashCode)
        {
            return m_container.GetConditional<TType>(hashCode);
        }

        public void Dispose()
        {
            m_container?.Dispose();
        }
    }
}