using System;
using DI.Factory;

namespace DI.Container
{
    public interface IDIContainer : IDisposable
    {
        public void Register<TType, TImplementation>();
        public void Register<TType, TImplementation>(IDIFactory<TImplementation> factory);
        public void RegisterConditional<TType, TImplementation>(IDIFactory<TImplementation> factory, int hashCode);

        public void RegisterInstance<TType, TImplementation>()
            where TImplementation : TType, new();

        public void RegisterInstance<TType, TImplementation>(TImplementation instance) where TImplementation : TType;
        public void RegisterInstanceConditional<TType, TImplementation>(TImplementation instance, int hashCode);
        public void Unregister<TType>();
        public void UnregisterConditional<TType>(int hashCode);
        public TType Get<TType>();
        public TType GetConditional<TType>(int hashCode);
    }
}