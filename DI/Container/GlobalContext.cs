namespace DI.Container
{
    public class GlobalContext : SimpleContext
    {
        public static GlobalContext instance
        {
            get
            {
                if (m_instance == null)
                {
                    m_instance = new GlobalContext();
                }

                return m_instance;
            }
        }

        private static GlobalContext m_instance;

        private GlobalContext()
        { }
    }
}