using System;
using System.Collections.Generic;
using DI.Factory;

namespace DI.Container
{
    public class DIContainer : IDIContainer
    {
        public const int DEFAULT_HASH = 0;

        private readonly Dictionary<int, Dictionary<int, Func<object>>> m_registrations =
            new Dictionary<int, Dictionary<int, Func<object>>>();

        public void Register<TType, TImplementation>()
        {
            IDIFactory<TImplementation> defaultFactory = new DefaultDIFactory<TImplementation>();
            Register<TType, TImplementation>(defaultFactory);
        }

        public void Register<TType, TImplementation>(IDIFactory<TImplementation> factory)
        {
            RegisterConditional<TType, TImplementation>(factory, DEFAULT_HASH);
        }

        public void RegisterConditional<TType, TImplementation>(IDIFactory<TImplementation> factory, int hashCode)
        {
            RegisterType<TType, TImplementation>(hashCode, () => factory.Create());
        }

        public void RegisterInstance<TType, TImplementation>() where TImplementation : TType, new()
        {
            RegisterInstance<TType, TImplementation>(Activator.CreateInstance<TImplementation>());
        }

        public void RegisterInstance<TType, TImplementation>(TImplementation instance) where TImplementation : TType
        {
            RegisterInstanceConditional<TType, TImplementation>(instance, DEFAULT_HASH);
        }

        public void RegisterInstanceConditional<TType, TImplementation>(TImplementation instance, int hashCode)
        {
            RegisterType<TType, TImplementation>(hashCode, () => instance);
        }

        public void Unregister<TType>()
        {
            UnregisterConditional<TType>(DEFAULT_HASH);
        }

        public void UnregisterConditional<TType>(int hashCode)
        {
            Type keyType = typeof(TType);
            int typeCode = keyType.GetHashCode();

            if (m_registrations.ContainsKey(typeCode) && m_registrations[typeCode].ContainsKey(hashCode))
            {
                m_registrations[typeCode].Remove(hashCode);
            }
            else
            {
                throw new ArgumentException($"Type {keyType.Name} are not registered");
            }
        }

        public TType Get<TType>()
        {
            return GetConditional<TType>(DEFAULT_HASH);
        }

        public TType GetConditional<TType>(int hashCode)
        {
            var typeCode = typeof(TType).GetHashCode();

            if (m_registrations.ContainsKey(typeCode) && m_registrations[typeCode].ContainsKey(hashCode))
            {
                return (TType) m_registrations[typeCode][hashCode].Invoke();
            }

            throw new ArgumentException($"There is no registration for {typeof(TType).Name}");
        }

        private void RegisterType<TType, TImplementation>(int hash, Func<object> creationFunc)
        {
            Type keyType = typeof(TType);
            int typeCode = keyType.GetHashCode();

            if (!keyType.IsInterface)
            {
                throw new ArgumentException($"Type {keyType.Name} is not interface so cant be registration key");
            }

            Type resultType = typeof(TImplementation);

            if (resultType.IsInterface || resultType.IsAbstract)
            {
                throw new ArgumentException(
                                            $"Type {resultType.Name} is an interface or an abstract class so cant be registration value"
                                           );
            }

            if (!m_registrations.ContainsKey(typeCode))
            {
                m_registrations.Add(typeCode, new Dictionary<int, Func<object>>());
            }

            if (!m_registrations[typeCode].ContainsKey(hash))
            {
                m_registrations[typeCode].Add(hash, creationFunc);
            }
            else
            {
                throw new ArgumentException($"Type {keyType.Name} already registered");
            }
        }

        public void Dispose()
        {
            m_registrations.Clear();
        }
    }
}