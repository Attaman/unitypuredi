namespace DI.Factory
{
    public interface IDIFactory<T>
    {
        public T Create();
    }
}