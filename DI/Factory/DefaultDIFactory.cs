using System;

namespace DI.Factory
{
    public class DefaultDIFactory<T>:IDIFactory<T>
    {
        public T Create()
        {
            return Activator.CreateInstance<T>();
        }
    }
}