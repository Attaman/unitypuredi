﻿using DI.Container;

namespace DI.Repository
{
    public interface IRepository<T> where T : IContext
    {
        void Init(T context);
    }
}