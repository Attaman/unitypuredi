﻿using System;
using DI.Container;
using UnityEngine;

namespace DI.Repository
{
    public abstract class AbstractMonoRepository : MonoBehaviour
    {
        public abstract void Init();
    }
    
    public abstract class AbstractMonoRepository<T> : AbstractMonoRepository, IRepository<T> where T : IContext
    {
        private bool m_inited;
        private void Start()
        {
            Init();
        }

        private void OnDestroy()
        {
            Dispose(GlobalContext.instance.Get<T>());
        }

        public override void Init()
        {
            if (!m_inited)
            {
                Init(GlobalContext.instance.Get<T>());
                m_inited = true;
            }
        }
        public abstract void Init(T context);

        protected abstract void Dispose(T context);
    }
}